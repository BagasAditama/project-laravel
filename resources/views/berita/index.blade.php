@extends('layout.master')

@section('judul')
Halaman ListBerita    
@endsection

@section('content')

<a href="/berita/create" class="btn btn-primary my-2">Tambah</a>

<div class="row">
    @forelse ($berita as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img src="{{asset('gambar/'. $item->thumbnail)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h3>{{$item->judul}}</h3>
              <p class="card-text">{{Str::limit($item->content, 30)}}</p>
              <form action="/berita/{{$item->id}}" method="POST">
                @csrf
                @method('DELETE')
                <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger btn sm">
              </form>
              
            </div>
          
    </div>
</div>  
    @empty
        <h4>Data Berita Kosong</h4>
    @endforelse

</div>
    



@endsection