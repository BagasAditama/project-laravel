@extends('layout.master')

@section('judul')
Halaman Edit Kategori {{$kategori->nama}}    
@endsection

@section('content')

<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf 
    @method('PUT')
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="nama" value="{{$kategori->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>Deskripsi</label>
    <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{$kategori->deskripsi}}</textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection